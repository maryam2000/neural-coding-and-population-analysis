# Neural Coding and Population Analysis

- This includes three of my Advance Topics and Computational Neuroscience Course Simulations.

- **Simulation 1:** Simulated and analyzed Leaky Integrate and Fire Neuron model (Shadlen and Newsome 1994) compared to Standard Integrate and Fire Neuron (Softky and Koch 1993).  

- **Simulation 2:** Studied the population response structure, analyzing the activity of a population of single units recorded with a multi-electrodes array in parietal cortex, Applied Principal Component Analysis for dimensionality reduction, Applied Shuffle tests to check the validity of results.

- **Simulation 3:** Effect of noise in population encoding and decoding, investigated the dependence of noise correlation on electrodes' distance for pairs grouped based on their orientation tuning similarity (data derived from Mattew A. Smith and Adam Kohn 2008)

A .zip file is uploaded for each Simulation, which includes all the necessary MATLAB .m files and a project report.

